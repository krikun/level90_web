CKEDITOR.editorConfig = (config) ->
  config.language = 'ru'

  config.filebrowserBrowseUrl = "/ckeditor/attachment_files";

  config.filebrowserFlashBrowseUrl = "/ckeditor/attachment_files";

  config.filebrowserFlashUploadUrl = "/ckeditor/attachment_files";

  config.filebrowserImageBrowseLinkUrl = "/ckeditor/pictures";

  config.filebrowserImageBrowseUrl = "/ckeditor/pictures";

  config.filebrowserImageUploadUrl = "/ckeditor/pictures";

  config.filebrowserUploadUrl = "/ckeditor/attachment_files";

  config.allowedContent = true;

  # config.extraPlugins = 'embed'
  config.toolbar_Pure = [
    { name: 'document',    items: [ 'Source'] },
    { name: 'clipboard',   items: [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
    { name: 'basicstyles', items: [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
    { name: 'paragraph',   items: [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
    { name: 'links',       items: [ 'Link','Unlink','Anchor' ] },
    { name: 'insert',      items: [ 'Image','Table','HorizontalRule','SpecialChar','Iframe' ] },
  ]
  config.toolbar = 'Pure'

  true
