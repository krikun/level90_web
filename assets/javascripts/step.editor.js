// === step ctrl logic ===
(function () {


var Step = function(element, index, steps) {
  this.element = element;
  this.index = index;
  this.steps = steps;
  this.init();
};


Step.prototype.init = function() {
  var ctrl = this.element.querySelector('.step-ctrl'),
      up = ctrl.querySelector('.up'),
      down = ctrl.querySelector('.down'),
      edit = ctrl.querySelector('.edit'),
      del = ctrl.querySelector('.delete');

  up.addEventListener('click', this.moveUp.bind(this));
  down.addEventListener('click', this.moveDown.bind(this));
  del.addEventListener('click', this.remove.bind(this));
};


Step.prototype.moveUp = function () {
  if (this.index > 0) {
    var self = this;
    $.ajax({
      url: "/steps/" + $(self.steps.steps[self.index].element).attr("id") + "/change_position",
      type: 'POST',
      data: { "position": self.index },
      success: function() {
        self.steps.swap(self.index - 1, self.index);

        return false;
      },
      fail: function(){
        console.log("something went wrong");
      }
    });
  }
};


Step.prototype.moveDown = function () {
  if (this.index + 1 < this.steps.steps.length) {
    var self = this;
    $.ajax({
      url: "/steps/" + $(self.steps.steps[self.index + 1].element).attr("id") + "/change_position",
      type: 'POST',
      data: { "position": self.index },
      success: function() {
        self.steps.swap(self.index, self.index + 1);

        return false;
      },
      fail: function(){
        console.log("something went wrong");
      }
    });
  }
};


Step.prototype.remove = function () {
  this.steps.remove(this.index);
}


var Steps = function(element, elements) {
  this.element = element;
  this.steps = this.init(elements);
};


Steps.prototype.init = function(elements) {
  var that = this;
  var steps = [];
  Array.prototype.forEach.call(elements, function(element, index) {
    steps.push(new Step(element, index, that));
  });
  return steps;
};


Steps.prototype.swap = function(index1, index2) {
  // // swap numbers
  this.swapNumbers(index1, index2);

  var prev = this.steps[index1],
      next = this.steps[index2];
  prev.index++;
  next.index--;

  // then move dom structure
  prev.element.parentNode.insertBefore(next.element, prev.element);
  this.steps[index1] = next;
  this.steps[index2] = prev;
};


Steps.prototype.swapNumbers = function(index1, index2) {
  var one = this.steps[index1].element.querySelector('.step-number');
  var two = this.steps[index2].element.querySelector('.step-number');
  var tmp = one.innerHTML;
  one.innerHTML = two.innerHTML;
  two.innerHTML = tmp;
};


Steps.prototype.remove = function(index) {
  var count = this.steps.length;
  for (var i=index+1; i<count; i++) {
    this.steps[i].index--;
  }

  var self = this;
  $.ajax({
    url: "/steps/" + $(self.steps[index].element).attr('id'),
    type: 'DELETE',
    success: function() {
      self.steps[index].element.remove();
      self.steps.splice(index, 1);
    },
    fail: function(){
      console.log("something went wrong");
    }
  });

};


// init
var steps = [];
var stepsElements = document.querySelectorAll('.steps');
Array.prototype.forEach.call(stepsElements, function(step) {
  var stepElements = step.querySelectorAll('.step');
  steps.push(new Steps(step, stepElements));
});



})();
