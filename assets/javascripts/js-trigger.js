$(function(){

	$(".js-trigger").each(function(index, el) {
		var label = $(this);
		var form = label.closest('form');
		var input = $("input", label);

		$("<div class='checker'></div>").appendTo(label);
		var cheker = $("div.checker", label);


		if (label.hasClass('active')) {
			input.trigger('click');
		}
		switch (input.attr("type")) {
			case "radio":
				label.addClass('radio');
				if (label.hasClass('disabled')) {
					return;
				}
				input.click(function(event) {
					if (label.hasClass('active')) {
						label.removeClass('active');
						input.attr('checked', false);
					}
				});
				input.change(function(evenfind) {
            form.find('.js-trigger').removeClass('active');
            label.addClass('active');
				});
				break
			case "checkbox":
				label.addClass('checkbox');
				if(label.hasClass('disabled')){
					return;
				}
				input.change(function(event) {
					if(label.hasClass('disabled')){
						input.removeAttr('checked');
					}else{
						label.toggleClass('active');
					}
				});
				break
		}
	});

});
