// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require ckeditor/init
//= require cocoon
//= require right-menu
//= require_tree .

$(function() {
  toggle_answers();
  $(document).on("change", "#step_task_attributes_type", toggle_answers);

  function toggle_answers(){
    var question_type = $("#step_task_attributes_type").val();
    console.log(question_type);

    if(question_type == "ChoiceTask") {
      $(".answer_options").removeClass("hidden");
    } else {
      $(".answer_options").addClass("hidden");
    }
  }
});
