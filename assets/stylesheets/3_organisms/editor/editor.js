(function() {

var Episode = function(element, index, episodes) {
	this.element = element;
	this.index = index;
	this.episodes = episodes;
	this.init();
};

Episode.prototype.init = function() {
	var toggle = this.element.querySelector('.episode-toggle');

	toggle.addEventListener('click', this.toggle.bind(this));

	var ctrl = this.element.querySelector('.episode-ctrl'),
			up = ctrl.querySelector('.up'),
			down = ctrl.querySelector('.down'),
			edit = ctrl.querySelector('.edit'),
			del = ctrl.querySelector('.delete');
	
	up.addEventListener('click', this.moveUp.bind(this));		
	down.addEventListener('click', this.moveDown.bind(this));		
	del.addEventListener('click', this.remove.bind(this));
};

Episode.prototype.toggle = function() {
	this.element.classList.toggle('open');
};

Episode.prototype.moveUp = function () {
	if (this.index > 0) {
		this.episodes.swap(this.index - 1, this.index);
	}
};

Episode.prototype.moveDown = function () {
	if (this.index + 1 < this.episodes.episodes.length) {
		this.episodes.swap(this.index, this.index + 1);
	}
};

Episode.prototype.remove = function () {
	this.episodes.remove(this.index);
}

var Episodes = function(element, elements) {
	this.element = element;
	this.episodes = this.init(elements);
};

Episodes.prototype.init = function(elements) {
	var that = this;
	var episodes = [];
	Array.prototype.forEach.call(elements, function(element, index) {
		episodes.push(new Episode(element, index, that));
	});
	return episodes;
};

Episodes.prototype.swap = function(index1, index2) {
	var prev = this.episodes[index1],
			next = this.episodes[index2];
	prev.index++;
	next.index--;
	this.element.insertBefore(next.element, prev.element);
	this.episodes[index1] = next;
	this.episodes[index2] = prev;
};

Episodes.prototype.remove = function(index) {
	var count = this.episodes.length;
	for (var i=index+1; i<count; i++) {
		this.episodes[i].index--;
	}
	this.episodes[index].element.remove();
	this.episodes.splice(index, 1);
};

var episodesParent = document.querySelector('.episodes');
var episodesElements = document.querySelectorAll('.episode');
var episodes = new Episodes(episodesParent, episodesElements);

})();


(function(){

var Step = function(element, index, steps) {
	this.element = element;
	this.index = index;
	this.steps = steps;
	this.init();
};

Step.prototype.init = function() {
	var ctrl = this.element.querySelector('.step-ctrl'),
			up = ctrl.querySelector('.up'),
			down = ctrl.querySelector('.down'),
			edit = ctrl.querySelector('.edit'),
			del = ctrl.querySelector('.delete');
	
	up.addEventListener('click', this.moveUp.bind(this));		
	down.addEventListener('click', this.moveDown.bind(this));		
	del.addEventListener('click', this.remove.bind(this));
};

Step.prototype.moveUp = function () {
	if (this.index > 0) {
		this.steps.swap(this.index - 1, this.index);
	}
};

Step.prototype.moveDown = function () {
	if (this.index + 1 < this.steps.steps.length) {
		this.steps.swap(this.index, this.index + 1);
	}
};

Step.prototype.remove = function () {
	this.steps.remove(this.index);
}

var Steps = function(element, elements) {
	this.element = element;
	this.steps = this.init(elements);
};

Steps.prototype.init = function(elements) {
	var that = this;
	var steps = [];
	Array.prototype.forEach.call(elements, function(element, index) {
		steps.push(new Step(element, index, that));
	});
	return steps;
};

Steps.prototype.swap = function(index1, index2) {
	var prev = this.steps[index1],
			next = this.steps[index2];
	prev.index++;
	next.index--;
	prev.element.parentNode.insertBefore(next.element, prev.element);
	this.steps[index1] = next;
	this.steps[index2] = prev;
};

Steps.prototype.remove = function(index) {
	var count = this.steps.length;
	for (var i=index+1; i<count; i++) {
		this.steps[i].index--;
	}
	this.steps[index].element.remove();
	this.steps.splice(index, 1);
};

var steps = [];
var stepsElements = document.querySelectorAll('.steps');
Array.prototype.forEach.call(stepsElements, function(step) {
	var stepElements = step.querySelectorAll('.step');
	steps.push(new Steps(step, stepElements));
});

})();