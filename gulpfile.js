'use strict';

var gulp = require('gulp-help')(require('gulp')),
    sass = require('gulp-sass'),
    jade = require('gulp-jade'),
    del = require('del'),
    replace = require('gulp-replace'),
    runSequence = require('run-sequence');


// PREPARING move to rails repository

// strictly speaking, shell clean, copy-scripts will be shorter
// and all other stuff you want to too,
// but I don't know how do it platform independent

// $ rm -rf to-rails/
gulp.task('clean:to-rails', false, function () {
  return del.sync('to-rails/**');
});

// $ mkdir to-rails/
// $ cp assets/ to-rails/
gulp.task('copy:to-rails', false, function () {
  return gulp.src('assets/**/*.*')
    .pipe(gulp.dest('to-rails/'));
});

// As soon as we use font-awesome (for independent updates)
// (may be it's unnecessary, but I don't know)

// $ mkdir -p to-rails/fonts/font-awesome/
// $ cp node_modules/font-awesome/fonts/ to-rails/fonts/font-awesome
gulp.task('copy-fa:to-rails', false, function () {
  return gulp.src('node_modules/font-awesome/fonts/**/*')
    .pipe(gulp.dest('to-rails/fonts/font-awesome'));
});


// RAILS specific conversions

// in rails font-awesome include as gem
gulp.task('change:fa', false, function () {
  return gulp.src('to-rails/stylesheets/sheet.scss')
    .pipe(replace('"0_utils/font-awesome/font-awesome"', '"font-awesome"'))
    .pipe(gulp.dest('to-rails/stylesheets/'));
});

// instead of url, rails use special route system
// so change all url to image-url tags
gulp.task('change:url', false, function () {
  return gulp.src('assets/stylesheets/**/*.scss')
    .pipe(replace('image: url', 'image: image-url'))
    .pipe(gulp.dest('to-rails/stylesheets/'));
});


// PREPARE to build src with font-awesome and jquery (latest version)

// $ rm -rf dist/
gulp.task('clean:dist', false, function () {
  return del.sync('dist/**');
});

// In main scss file, we import font-awesome src,
// to use icons in our classes in scss as extension
//
// We cannot install npm packages to custom path, inside project,
// http://stackoverflow.com/questions/21818701/use-a-custom-directory-name-instead-of-node-modules-when-installing-with-npm
// You can say we can use bower and install it in specific directory,
// but lets use only npm and just copy it.
// To use shorter path to font-awesome src
// or we can use font-awesome as standalone css file.
// But in this case we wouldn't use font-awesome mixins and styles
// as our code extensions..

// $ mkdir assets/stylesheets/0_utils/font-awesome/
// $ cp node_modules/font-awesome/scss/* assets/stylesheets/0_utils/font-awesome/
gulp.task('copy-fa:to-assets', false, function () {
  return gulp.src('node_modules/font-awesome/scss/**/*')
  	.pipe(gulp.dest('assets/stylesheets/0_utils/font-awesome/'));
});

// $ mkdir assets/javascript/jquery
// $ cp node_modules/jquery/dist/* assets/javascript/jquery/
gulp.task('copy-jquery:to-assets', false, function () {
  return gulp.src('node_modules/jquery/dist/*')
  	.pipe(gulp.dest('assets/javascripts/jquery/'));
});

// Copy all files, images, javascript, fonts, even scss etc.
// $ mkdir dist/
// $ cp assets/ dist/
gulp.task('copy:dist', false, function () {
  return gulp.src('assets/**/*.*')
    .pipe(gulp.dest('dist/'));
});

// $ mkdir -p dist/fonts/font-awesome/
// $ cp node_modules/font-awesome/fonts/* dist/fonts/font-awesome/
gulp.task('copy-fa:dist', false, function () {
  return gulp.src('node_modules/font-awesome/fonts/**/*')
    .pipe(gulp.dest('dist/fonts/font-awesome'));
});


// DISTRIBUTION tasks

gulp.task('sass', 'Build css from assets/stylesheets/', function () {
  return gulp.src('assets/stylesheets/sheet.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('dist/stylesheets/'));
});

gulp.task('sass:watch', false, function () {
  gulp.watch('assets/stylesheets/**/*.scss', ['sass']);
});

gulp.task('jade', 'Build html from jade templates', function() {
  gulp.src('jade/*.jade')
    .pipe(jade())
    .pipe(gulp.dest('./dist'))
});

gulp.task('jade:watch', false, function () {
  gulp.watch('jade/**/*.jade', ['jade']);
});


// SEQUENCES

gulp.task('to-rails', 'Prepare src for rails project', function () {
  return runSequence('clean:to-rails', 'copy:to-rails', 'copy-fa:to-rails', 'change:fa', 'change:url');
});

gulp.task('build', 'Build html and css', function () {
  return runSequence('clean:dist', 'copy-fa:to-assets', 'copy-jquery:to-assets', 'copy:dist', 'copy-fa:dist', 'sass', 'jade');
});

gulp.task('watch', 'Build html and css on changes', ['sass:watch', 'jade:watch']);

gulp.task('default', ['build', 'watch']);


