$(".menu-init").each(function(){
	var link = $(this);
	var container = $(".navigation");
	var close = $(".navigation__right-menu-cross");
	var bg = $(".navigation__bg");


	link.click(function(event) {
		event.preventDefault();
		event.stopPropagation();
		container.addClass('active');
	});	
	close.add(bg).click(function(event) {
		event.preventDefault();
		event.stopPropagation();
		container.removeClass('active');
	});	
});