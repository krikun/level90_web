(function() {

/**
 * Toggable add to element .open
 * onclick by [data-toggle] inside element
 */

var Toggable = function(element, toggleSelector) {
  this.element = element;
  var toggle = this.element.querySelector(toggleSelector);
  toggle.addEventListener('click', this.toggle.bind(this));
};

Toggable.prototype.toggle = function() {
  this.element.classList.toggle('open');
};


var Episode = function(element, index, episodes) {
  this.element = element;
  this.index = index;
  this.episodes = episodes;
  this.name = element.querySelector('.episode-text');

  // elements selector .episode-toggle
  this.toggle = new Toggable(element, '.episode-toggle');
  this.initCtrl();
};


Episode.prototype.initCtrl = function() {
  var ctrl = this.element.querySelector('.episode-ctrl'),
      up = ctrl.querySelector('.up'),
      down = ctrl.querySelector('.down'),
      edit = ctrl.querySelector('.edit'),
      del = ctrl.querySelector('.delete');

  up.addEventListener('click', this.moveUp.bind(this));
  down.addEventListener('click', this.moveDown.bind(this));
  edit.addEventListener('click', this.edit.bind(this));
  del.addEventListener('click', this.remove.bind(this));
};


Episode.prototype.moveUp = function () {
  if (this.index > 0) {
    var self = this;

    $.ajax({
      url: "/episodes/" + $(self.episodes.episodes[self.index].element).attr("id") + "/change_position",
      type: 'POST',
      data: { "position": self.index },
      success: function() {
        self.episodes.swap(self.index - 1, self.index);
        return false;
      },
      fail: function() {
        console.log("something went wrong");
      }
    });
  }
};


Episode.prototype.moveDown = function () {
  if (this.index + 1 < this.episodes.episodes.length) {
    var self = this;

    $.ajax({
      url: "/episodes/" + $(self.episodes.episodes[self.index + 1].element).attr("id") + "/change_position",
      type: 'POST',
      data: { "position": self.index },
      success: function() {
        self.episodes.swap(self.index, self.index + 1);
        return false;
      },
      fail: function(){
        console.log("something went wrong");
      }
    });
  }
};


Episode.prototype.change = function (name) {
  this.name.innerHTML = name;
}


Episode.prototype.edit = function () {
  this.episodes.showEditor(this.index);
}


Episode.prototype.remove = function () {
  this.episodes.remove(this.index);
}


// DDD!
// should create domen and everything in it

// seems as a lot of js dom manipulations
// also bad (should use some library)

var Episodes = function(element, elements) {
  // different names with actions and scripts
  // should draw that and change
  this.element = element;
  this.template = element.querySelector('.episode-template');
  this.form = element.querySelector('.episode-form');
  this.link = element.querySelector('.episode-link');
  this.submitButton = this.form.querySelector('.submit');
  this.cancelButton = this.form.querySelector('.cancel');
  this.course = this.submitButton.dataset.course;
  this.changeId = null;

  this.link.addEventListener('click', this.showEditor.bind(this));
  this.cancelButton.addEventListener('click', this.hideEditor.bind(this));
  this.submitButton.addEventListener('click', this.save.bind(this));
  this.episodes = this.init(elements);
};

Episodes.prototype.loading = function(loaded) {
  if (!loaded) {
    this.cancelButton.querySelector('i').className = "fa fa-spinner fa-spin";
    this.submitButton.disabled = true;
  } else {
    this.cancelButton.querySelector('i').className = "fa fa-times";
    this.submitButton.disabled = false;
  }
}


Episodes.prototype.init = function(elements) {
  var that = this;
  var episodes = [];
  Array.prototype.forEach.call(elements, function(element, index) {
    episodes.push(new Episode(element, index, that));
  });
  return episodes;
};


Episodes.prototype.addEpisode = function(name, episodeId) {
  var newEpisode = this.template.cloneNode(true);
  var newEpisodeText = newEpisode.querySelector('.episode-text');
  var newEpisodeNumber = newEpisode.querySelector('.episode-number');
  newEpisodeNumber.innerHTML = newEpisodeNumber.innerHTML + " " + (episodes.episodes.length + 1) + ". ";
  newEpisodeText.innerHTML = name;
  newEpisode.className = 'episode open';
  newEpisode.id = episodeId;
  this.template.parentNode.insertBefore(newEpisode, this.template);
  this.hideEditor();
  this.episodes.push(new Episode(newEpisode, this.episodes.length, this));
}


Episodes.prototype.changeEpisode = function(index, name) {
  this.episodes[index].change(name);
  this.hideEditor();
}


Episodes.prototype.swapLabels = function(index1, index2) {
  var one = this.episodes[index1].element.querySelector('.episode-number');
  var two = this.episodes[index2].element.querySelector('.episode-number');

  var tmp = one.innerHTML;
  one.innerHTML = two.innerHTML;
  two.innerHTML = tmp;
};


Episodes.prototype.swap = function(index1, index2) {
  this.swapLabels(index1, index2);

  var prev = this.episodes[index1],
      next = this.episodes[index2];
  prev.index++;
  next.index--;

  this.element.insertBefore(next.element, prev.element);
  this.episodes[index1] = next;
  this.episodes[index2] = prev;
};


Episodes.prototype.remove = function(index) {
  var count = this.episodes.length;
  for (var i=count-1; i>index; i--) {
    this.episodes[i].index--;
    this.swapLabels(i, i-1);
  }

  var self = this;
  $.ajax({
    url: "/episodes/" + $(self.episodes[index].element).attr('id'),
    type: 'DELETE',
    success: function() {
      self.episodes[index].element.remove();
      self.episodes.splice(index, 1);
    },
    fail: function(){
      console.log("something went wrong");
    }
  });
};


Episodes.prototype.showEditor = function(index) {
  // reset hidden fields
  this.hideEditor();
  // init like without index
  // in end of the list
  var name = '';

  if (index >= 0) {
    var before = this.episodes[index];
    name = before.name.innerHTML;
    this.element.insertBefore(this.form, before.element);
    before.element.querySelector('.episode-label').classList.add('hidden');
    this.submitButton.innerHTML = "Изменить";
    this.changeId = index;
  } else {
    this.element.insertBefore(this.form, this.template);
    this.link.classList.add('hidden');
    this.submitButton.innerHTML = "Добавить";
    this.changeId = null;
  }

  // set and focus input
  var input = this.form.querySelector('input');
  input.value = name;

  this.form.classList.add('active');

  input.focus();
};


Episodes.prototype.hideEditor = function() {
  var hidden = this.element.querySelectorAll('.hidden');
  Array.prototype.forEach.call(hidden, function (element) {
    element.classList.remove('hidden');
  });
  this.form.classList.remove('active');
};


Episodes.prototype.save = function() {
  var that = this;
  var name = this.form.querySelector('input').value;

  // add new episode
  if (this.changeId==null) {
    this.loading(false);
    $.ajax({
      url: "/" + this.course + "/episodes",
      type: 'POST',
      data: { "episode": { "title": name } },
      success: function(response) {
        // also should return course id (could be json)
        // for following delete action
        that.addEpisode(name, response.episode_id);
        that.loading(true);
      },
      fail: function(){
        that.loading(true);
        console.log("something went wrong");
      }
    });
  // change existing
  } else {
    that.changeEpisode(this.changeId, name);

    var episodeId = this.episodes[this.changeId].element.id;
    console.log(episodeId);

    this.loading(false);
    $.ajax({
      url: "/episodes/" + episodeId,
      type: 'PUT',
      data: { "episode": { "title": name } },
      success: function(response) {
        // also should return course id (could be json)
        // for following delete action
        that.loading(true);
      },
      fail: function(){
        that.loading(true);
        console.log("something went wrong");
      }
    });
  }

};


var episodesParent = document.querySelector('.episodes');
var episodesElements = document.querySelectorAll('.episode');
if (episodesParent) {
  var episodes = new Episodes(episodesParent, episodesElements);
  window.episodes = episodes;
}

})();
