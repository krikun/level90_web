# Level90 web application

[Rails application level90_client](https://gitlab.com/level90/level90_client)

_Planned mount to-rails folder in this repository_

## Dependencies

Install dependencies:

```bash
npm install # or npm i
```

## npm Tasks

Build distribution:

```
npm run build
```

Start local server:

```bash
npm start
```

## gulp Tasks

```
gulp
```

`default` task, as you can see, is `build` and `watch`

```
gulp help

Usage
  gulp [TASK] [OPTIONS...]

Available tasks
  build     Build html and css
  default   [build, watch]
  help      Display this help text.
  jade      Build html from jade templates
  sass      Build css from assets/stylesheets/
  to-rails  Prepare src for rails project
  watch     Build html and css on changes [sass:watch, jade:watch]
```

And hidden ones' - you can run it for specific action:

```bash
# Prepare directories and source
gulp clean:dist        # Clean distribution directory
gulp clean:to-rails    # Clean to-rails directory

# Copy src:
gulp copy:dist         # Copy assets to distribution directory
gulp copy:to-rails     # Copy assets before convert them to-rails

# Copy fonts:
gulp copy-fa:to-assets # Copy fonts to distribution directory
gulp copy-fa:to-rails  # Copy fonts to rails assets (may be unnecessary)

# Copy jquery latest:
gulp copy-jquery:to-assets

# Next one change some src to-rails:
gulp change:url        # Replace local url with image-url rails tag
gulp change:fa         # Replace local font-awesome with rails gem 

gulp sass              # Generate css from scss (sass)
gulp jade              # Compile jade to html (placed files in distribution)
```
